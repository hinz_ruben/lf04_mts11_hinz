public class Ersatzwiderstand{
	public static void main(String[] args) {
		double r1=123;
		double r2=50;
		double rGesR=reihenschaltung(r1,r2);
		double rGesP=parallelschaltung(r1,r2);
		System.out.printf("Der Ersatzwiderstand bei Reihenschaltung ist %.2f Ohm\n",rGesR);
		System.out.printf("Der Ersatzwiderstand bei Parallelschaltung ist %.2f Ohm",rGesP);
	
	}
	public static double reihenschaltung(double r1, double r2) {
		double erg=r1+r2;
		return erg;
	}
	public static double parallelschaltung(double r1, double r2) {
		double erg=(r1*r2)/(r1+r2);
		return erg;
	}
}