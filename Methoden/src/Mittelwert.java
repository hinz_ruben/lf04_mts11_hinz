public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m;
      m = berechneMittelwert(x, y);
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
   private static double berechneMittelwert(double x,double y){
	   double ergebnis=(x+y)/(double)2;
	   return ergebnis;
   }
}