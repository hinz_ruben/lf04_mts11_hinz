import java.util.Scanner;

public class HardwareHaendler {
	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		System.out.printf("Gebe den Einzelbetrag ein: ");
		double einzelpreis=eingabe.nextDouble();
		System.out.printf("Gebe die Anzahl ein: ");
		int anzahl=eingabe.nextInt();
		double gesamtpreis=0;
		if(anzahl>=10) {
			gesamtpreis=(anzahl*einzelpreis)*1.19;
		}else {
			gesamtpreis=((anzahl*einzelpreis)*1.19)+10;
		}
		System.out.printf("Der Gesamtpreis ist %.2f ",gesamtpreis);
	}
}