import java.util.Scanner;

public class Steuersatz {
	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		System.out.printf("Gebe den Nettobetrag ein: ");
		double nettowert=eingabe.nextDouble();
		System.out.printf("Waehle den ermaessigten Steuersatz mit 'j'/ voller Steuersatz mit 'n':");
		char steuersatz=eingabe.next().charAt(0);
		double bruttobetrag=0;
		if(steuersatz=='j') {
			bruttobetrag=nettowert*1.07;
		}else if(steuersatz=='n') {
			bruttobetrag=nettowert*1.19;
		}
		System.out.printf("Der Bruttobetrag betraegt %.2f Euro",bruttobetrag);
	}
}
