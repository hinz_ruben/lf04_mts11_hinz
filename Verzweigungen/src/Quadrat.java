import java.util.Scanner;

public class Quadrat {
	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.printf("Waehle die Seitenlaenge des Quadrats: ");
		int seitenlaenge=scan.nextInt();
		for (int i=0;i<seitenlaenge;i++) {
			if(i==0||i==seitenlaenge-1) {
				for(int j=0;j<seitenlaenge;j++) {
					System.out.printf("* ");
				}
				System.out.printf("\n");
			}else {
				System.out.printf("* ");
				for(int j=0;j<seitenlaenge-2;j++) {
					System.out.printf("  ");
				}
				System.out.printf("*\n");
			}
		}
		scan.close();
	}
}