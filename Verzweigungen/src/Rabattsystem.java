import java.util.Scanner;

public class Rabattsystem {
	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		System.out.printf("Gebe den Einzelbetrag ein: ");
		double einzelpreis=eingabe.nextDouble();
		System.out.printf("Gebe die Anzahl ein: ");
		int anzahl=eingabe.nextInt();
		double gesamtpreis=0;
		double bestellwert=0;
		if(anzahl>=10) {
			bestellwert=(anzahl*einzelpreis);
		}else {
			bestellwert=(anzahl*einzelpreis)+10;
		}
		System.out.printf("Der Bestellwert betraegt %.2f Euro\n",bestellwert);
		if(bestellwert<100&&bestellwert>0) {
			gesamtpreis=bestellwert*0.9;
		}else if(bestellwert<500) {
			gesamtpreis=bestellwert*0.85;
		}else {
			gesamtpreis=bestellwert*0.8;
		}
		gesamtpreis*=1.19;
		System.out.printf("Der Gesamtpreis mit Rabatt und MwSt ist %.2f ",gesamtpreis);
	}
}