import java.util.Scanner;

public class Treppe {
	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.printf("Treppenhoehe: ");
		int hoehe=scan.nextInt();
		System.out.printf("Stufenbreite: ");
		int breite=scan.nextInt();
		String stufe="";
		String keinestufe="";
		for(int k=0;k<breite;k++) {
			stufe +="*";
			keinestufe+=" ";
		}
		for(int i=hoehe-1;i>=0;i--) {
			for (int l=0;l<i;l++) {
				System.out.printf(keinestufe);
			}
			for (int m=0;m<hoehe-i;m++) {
				System.out.printf(stufe);
			}
			System.out.printf("\n");
		}
		scan.close();
	}
}