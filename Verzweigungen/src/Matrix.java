import java.util.Scanner;

public class Matrix {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.printf("Zahl von 2 bis 9: ");
		int zahl = scan.nextInt();
		if (zahl < 2 || zahl > 9) {
			System.out.println("Error");
		} else {
			int x = 0;
			while (x < 100) {
				if (x % zahl == 0 && x / zahl != 0) {
					System.out.printf("  *");
				} else if ((x % 10 + x / 10) == zahl) {
					System.out.printf("  *");
				} else if (x % 10 == zahl) {
					System.out.printf("  *");
				} else if (x / 10 == zahl) {
					System.out.printf("  *");
				} else if ((x - x % 10) % 10 == zahl) {
					System.out.printf("  *");
				} else {
					System.out.printf("%3d", x);
				}
				if (x % 10 == 9) {
					System.out.printf("\n");
				}
				x += 1;
			}
		}
	}
}