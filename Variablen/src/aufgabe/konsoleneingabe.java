package aufgabe;

import java.util.Scanner;

public class konsoleneingabe {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String name;
		int alter;
		System.out.printf("Wie ist dein Name? ");
		name=myScanner.next();
		System.out.printf("Wie alt bist du? ");
		alter=myScanner.nextInt();
		System.out.printf("Du bist %s und du bist %d Jahre alt!\n",name,alter);
		
		myScanner.close();
	}
}