﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		do {
			double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
			double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);
		} while (true);
		// tastatur.close();
	}

	public static double fahrkartenbestellungErfassen(Scanner tastatur) {
		//Die neue Implementierung ist deutlich benutzer/ änderungsfreundlicher, da nur die Arrays geändert werden müssen und nicht ein neuer case 
		//im switch geschrieben werden muss und andere Parameter angepasst werden müssten.
		int maxTicketAnzahl=10;
		int ticketAnzahl;
		int ticketArt;
		String[] ticketName=new String[] {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB",
				"Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
		double[] ticketPreise=new double[]{2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
		double ticketPreis = 0;
		boolean ungueltig = false;
		
		
		do {
			System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
			for(int i=0;i<ticketName.length;i++) {
				System.out.printf("%2d.  %-40s %5.2f \n",i+1,ticketName[i],ticketPreise[i]);
			}

			ticketArt = tastatur.nextInt();
			System.out.printf("Ihre Wahl: %d.  %s\n",ticketArt, ticketName[ticketArt-1]);
			if(ticketArt<ticketPreise.length+1&&ticketArt>0) {
				ticketPreis=ticketPreise[ticketArt-1];
			}else {
				System.out.println("Ungueltige Eingabe");
				ungueltig = true;
			}
		} while (ungueltig);
		do {
			System.out.print("Anzahl Tickets (Stueck): ");
			ticketAnzahl = tastatur.nextInt();
			if(ticketAnzahl>maxTicketAnzahl||ticketAnzahl<1) {
				System.out.printf(" >> Wählen Sie bitte eine Anzahl von 1 bis %d Tickets aus.\n",maxTicketAnzahl);
			}
		}while(ticketAnzahl>maxTicketAnzahl||ticketAnzahl<0);

		return ticketPreis * (double) ticketAnzahl;

	}

	public static double fahrkartenBezahlen(double zahlBetrag, Scanner tastatur) {
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMuenze;
		double[] muenzen=new double[] {500,200,100,50,20,10,5,2,1,0.5,0.2,0.1,0.05,0.02,0.01};
		while (eingezahlterGesamtbetrag < zahlBetrag) {
			System.out.printf("Noch zu zahlen: %.2f EURO\n", (zahlBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			if(contains(muenzen,eingeworfeneMuenze))
				eingezahlterGesamtbetrag += eingeworfeneMuenze;
			else
				System.out.println("Ungültiger einwurf");
		}
		return eingezahlterGesamtbetrag - zahlBetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.printf("\n Fahrschein(e) werden ausgegeben\n");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {
		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rueckgabebetrag);
			System.out.println("wird ausgezahlt mit:");
			double[] muenzen=new double[] {500,200,100,50,20,10,5,2,1,0.5,0.2,0.1,0.05,0.02,0.01};
			for(int i=0;i<muenzen.length;i++) {
				while (rueckgabebetrag >= muenzen[i]) { 
					rueckgabebetrag-=muenzen[i];
					if(i>6)
						printMuenze(muenzen[i]);
					else
						printSchein(muenzen[i]);
				}
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n\n");
	}
	
	public static void printMuenze(double muenze) {
		System.out.printf("%8s\n *%8s\n*%6.2f%4s\n *%8s\n%8s\n","* * *","*",muenze,"*","*","* * *");
	}
	public static void printSchein(double schein) {
		System.out.printf("***************\n*%14s\n*   %6.2f%5s\n*%14s\n***************\n","*",schein,"*","*");
	}
	public static boolean contains(double[] feld,double key) {
		for(int i=0;i<feld.length;i++) {
			if(feld[i]==key) {
				return true;
			}
		}
		return false;
	}
	
}



/*
 * 5.Für die Ticketanzahl habe ich den Datentyp int gewählt, da die Anzahl der
 * Tickets immer eine ganze Zahl sein sollte, bzw. ist.
 * 
 * 6.Bei der Berechnung des Ausdrucks ticketPreis*ticketAnzahl wird erst die
 * Ticketanzahl in double gecastet und dann die Multiplikation ausgeführt und
 * dem zuZahlendenBetrag zugewiesen.
 */